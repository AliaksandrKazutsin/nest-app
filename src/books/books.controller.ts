import {
  Controller,
  Delete,
  Put,
  Get,
  Param,
  Post,
  Body,
  Redirect,
  HttpCode,
  HttpStatus,
  Header,
  Req,
  Res,
} from '@nestjs/common';
import { BooksService } from './books.service';

import {CreateBookDto} from './dto/create-book.dto';
import {UpdateBookDto} from './dto/update-book.dto';
import { Book } from './schemas/book.schema';

//works with requests and redirects, makes params
@Controller('books')
export class BooksController {
//   getAllBooks(@Req() req: Request, @Res() res: Response): string {
// 	res.status(200).end('Text')
//     return 'getAllBooks';
//   }
  constructor(private readonly bookService: BooksService) {

  }

  @Get()
  //@Redirect('https://google.com', 301)
  getAllBooks(): Promise<Book[]> {
    return this.bookService.getAllBooks();
  }

  @Get(':id')
  getOneBook(@Param('id') id: string): Promise<Book> {
    return this.bookService.getBookById(id)
  }

  @Post()
  //@HttpCode(HttpStatus.CREATED)
  //@Header('Cache-Control', 'none')
  create(@Body() CreateBookDto: CreateBookDto): Promise<Book> {
	  return this.bookService.create(CreateBookDto)//`Book title: ${CreateBookDto.title}, Author: ${CreateBookDto.author}`;
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<Book> {
	  return this.bookService.remove(id)
  }

  @Put()
  update(@Body() updateBookDto: UpdateBookDto, @Param('id') id: string): Promise<Book> {
	  return this.bookService.update(id, updateBookDto)
  }
}
