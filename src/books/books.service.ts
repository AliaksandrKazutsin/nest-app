import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CreateBookDto } from "./dto/create-book.dto";
import { UpdateBookDto } from "./dto/update-book.dto";
import { Book, BookDocument } from "./schemas/book.schema";

//perfom all logic
@Injectable()
export class BooksService {
	constructor(@InjectModel(Book.name) private bookModel: Model<BookDocument>) {

	}

	async getAllBooks(): Promise<Book[]> {
		return this.bookModel.find().exec()
	}

	async getBookById(id: string): Promise<Book> {
		return this.bookModel.findById(id)
	}

	async create(bookDto: CreateBookDto): Promise<Book> {
		const newBook = new this.bookModel(bookDto)
		return newBook.save()
	}

	async remove(id: string): Promise<Book> {
		return this.bookModel.findByIdAndRemove(id)
	}

	async update(id: string, bookDto: UpdateBookDto): Promise<Book> {
		return this.bookModel.findByIdAndUpdate(id, bookDto, {new: true})
	}

	// private books = []

	// getAllBooks() {
	// 	return this.books
	// }

	// getBookById(id: string) {
	// 	return this.books.find(book => book.id === id)
	// }

	// create(bookDto: CreateBookDto) {
	// 	return this.books.push({
	// 			...bookDto,
	// 			id: Date.now().toString()
	// 		})
	// }
}
