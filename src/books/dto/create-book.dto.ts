export class CreateBookDto {
  readonly title: string;
  readonly author: string;
  readonly label: string;
  readonly description: string;
  readonly downloadLink: string;
}
